package com.example.demo.dto.builders;

import com.example.demo.dto.PatientDTO;
import com.example.demo.model.Patient;
import com.example.demo.model.User;

import java.util.Optional;

public class PacientBuilder {

    public PacientBuilder(){

    }

    public static PatientDTO generateDTOFromEntity(Patient patient){
        return new PatientDTO(patient.getId(),
                patient.getUsername(),
                patient.getPassword(),
                patient.getRole(),
                patient.getName(),
                patient.getBirthDate(),
                patient.getAdress(),
                patient.getGender(),
                patient.getMedicalRecord(),
                patient.getDoctor().getId(),
                patient.getCaregiver().getId());
    }

    public static Patient generateEntityFromDTO(PatientDTO patientDTO){
        return new Patient(patientDTO.getId(),
                patientDTO.getUsername(),
                patientDTO.getPassword(),
                patientDTO.getRole(),
                patientDTO.getName(),
                patientDTO.getBirthDate(),
                patientDTO.getAddress(),
                patientDTO.getGender(),
                patientDTO.getMedicalRecord());

    }

}
