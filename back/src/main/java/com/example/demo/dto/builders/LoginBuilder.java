package com.example.demo.dto.builders;

import com.example.demo.dto.LoginDTO;
import com.example.demo.model.User;

public class LoginBuilder {

    public static LoginDTO generateDTOFromEntity(User user){
        return new LoginDTO(user.getUsername(),
                user.getPassword());
    }
}
