import {Component, OnInit} from '@angular/core';
import {User} from '../models/user';
import {CaregiverService} from '../service/caregiver.service';
import {Router} from '@angular/router';
import {Patient} from '../models/patient';
import {PatientService} from '../service/patient.service';
import {UserService} from '../service/user.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-create-patient',
  templateUrl: './create-patient.component.html',
  styleUrls: ['./create-patient.component.css']
})
export class CreatePatientComponent implements OnInit {

  patient: Patient = new Patient();
  submitted = false;
  doctor: User;

  constructor(private patientService: PatientService,
              private caregiverService: CaregiverService,
              private userService: UserService,
              private router: Router) {
  }

  ngOnInit() {
  }

  save() {
    this.patient.role = 'PATIENT';
    this.patient.doctorId = 5;
    console.log(this.patient);
    this.patientService.createPatient(this.patient)
      .subscribe(
        data => {
          console.log(data);
          alert('Patient was created successfully');
        }, error => console.log(error));

    this.patient = new Patient();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['doctor']);
  }
}
