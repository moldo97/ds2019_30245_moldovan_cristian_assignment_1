import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {REST_API} from '../common/API';
import {LoginDTO} from '../models/user';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  login(user: LoginDTO): Observable<any> {
    return this.http.post(`${REST_API + 'user/login'}`, user);
  }

  getUser(username: string): Observable<any> {
    return this.http.get(`${REST_API + 'user'}/${username}`);
  }

}
