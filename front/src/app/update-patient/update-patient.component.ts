import { Component, OnInit } from '@angular/core';
import {User} from '../models/user';
import {ActivatedRoute, Router} from '@angular/router';
import {CaregiverService} from '../service/caregiver.service';
import {Patient} from '../models/patient';
import {PatientService} from '../service/patient.service';

@Component({
  selector: 'app-update-patient',
  templateUrl: './update-patient.component.html',
  styleUrls: ['./update-patient.component.css']
})
export class UpdatePatientComponent implements OnInit {

  id: number;
  patient: Patient;
  submitted = false;

  constructor(private route: ActivatedRoute, private router: Router,
              private patientService: PatientService) {
  }

  ngOnInit() {
    this.patient = new Patient();

    this.id = this.route.snapshot.params.id;

    this.patientService.getPatient(this.id)
      .subscribe(data => {
        console.log(data);
        this.patient = data;
      }, error => console.log(error));
  }

  updatePatient() {
    console.log(this.patient);
    this.patientService.updatePatient(this.patient, this.id)
      .subscribe(
        data => {
          console.log(data);
          alert('Patient was successfully updated');
        },
        error => console.log(error));
    this.patient = new Patient();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.updatePatient();
  }

  gotoList() {
    this.router.navigate(['doctor']);
  }

  back() {
    this.router.navigate(['/doctor/patient']);
  }

}
