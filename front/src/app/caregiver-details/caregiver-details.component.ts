import { Component, OnInit } from '@angular/core';
import {Medication} from '../models/medication';
import {ActivatedRoute, Router} from '@angular/router';
import {MedicationService} from '../service/medication.service';
import {User} from '../models/user';
import {UserService} from '../service/user.service';
import {CaregiverService} from '../service/caregiver.service';

@Component({
  selector: 'app-caregiver-details',
  templateUrl: './caregiver-details.component.html',
  styleUrls: ['./caregiver-details.component.css']
})
export class CaregiverDetailsComponent implements OnInit {

  id: number;
  user: User;

  constructor(private route: ActivatedRoute, private router: Router,
              private caregiverService: CaregiverService) { }

  ngOnInit() {
    this.user = new User();

    this.id = this.route.snapshot.params.id;

    this.caregiverService.getCaregiver(this.id)
      .subscribe(data => {
        console.log(data);
        this.user = data;
      }, error => console.log(error));
  }

  list() {
    this.router.navigate(['doctor/caregiver']);
  }

}
