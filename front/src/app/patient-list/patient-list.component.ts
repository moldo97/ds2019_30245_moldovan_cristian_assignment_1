import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../models/user';
import {CaregiverService} from '../service/caregiver.service';
import {Router} from '@angular/router';
import {Patient} from '../models/patient';
import {PatientService} from '../service/patient.service';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {

  patient: Observable<Patient[]>;

  constructor(private patientService: PatientService,
              private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.patient = this.patientService.getPatientDoctorList(sessionStorage.getItem('username'));

  }

  deletePatient(id: number) {
    this.patientService.deletePatient(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  patientDetails(id: number) {
    this.router.navigate(['patientDetails', id]);
  }

  updatePatient(id: number, patient: Patient) {
    this.router.navigate(['updatePatient', id, patient]);
  }

  back() {
    this.router.navigate(['/doctor']);
  }

}
