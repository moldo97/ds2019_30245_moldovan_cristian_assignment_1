import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-caregiver',
  templateUrl: './caregiver.component.html',
  styleUrls: ['./caregiver.component.css']
})
export class CaregiverComponent implements OnInit {
  name: string;

  constructor(private router: Router) { }

  ngOnInit() {
    this.name = sessionStorage.getItem('name');
  }

  logout() {
    this.router.navigate(['login']);
    sessionStorage.setItem('role', '');
    sessionStorage.setItem('name', '');
    sessionStorage.setItem('username', '');
  }
}
