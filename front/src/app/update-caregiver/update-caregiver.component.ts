import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../models/user';
import {CaregiverService} from '../service/caregiver.service';

@Component({
  selector: 'app-update-caregiver',
  templateUrl: './update-caregiver.component.html',
  styleUrls: ['./update-caregiver.component.css']
})
export class UpdateCaregiverComponent implements OnInit {

  id: number;
  user: User;
  submitted = false;

  constructor(private route: ActivatedRoute, private router: Router,
              private caregiverService: CaregiverService) {
  }

  ngOnInit() {
    this.user = new User();

    this.id = this.route.snapshot.params.id;

    this.caregiverService.getCaregiver(this.id)
      .subscribe(data => {
        console.log(data);
        this.user = data;
      }, error => console.log(error));
  }

  updateCaregiver() {
    console.log(this.user);
    this.caregiverService.updateCaregiver(this.id, this.user)
      .subscribe(
        data => {
          console.log(data);
          alert('Caregiver was successfully updated');
        },
        error => console.log(error));
    this.user = new User();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.updateCaregiver();
  }

  gotoList() {
    this.router.navigate(['doctor']);
  }

  back() {
    this.router.navigate(['/doctor/caregiver']);
  }

}
